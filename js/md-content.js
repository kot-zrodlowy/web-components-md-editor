// import markdown from 'markdown-it';

class MarkDownContent extends HTMLElement {
	constructor() {
		super();
		this.content = this.getRawContent();
		this.innerHTML = this.compileSource(this.content.trim() || this.placeholder);
		if(!this.disabled && this.editable){
			this.setAttribute('tabindex', 0);
			this.addEventListener('focus', this.focusCallback);
			this.addEventListener('blur', this.blurCallback);
		}
	}

	get disabled() {
		return this.hasAttribute('disabled');
	}
	
	set disabled(val) {
		if (val) {
			this.setAttribute('disabled', '');
		} else {
			this.removeAttribute('disabled');
		}
	}

	get editable() {
		return this.hasAttribute('editable');
	}

	set editable(value) {
		if (val) {
			this.setAttribute('editable', '');
		} else {
			this.removeAttribute('editable');
		}
	}
	get placeholder() {
		return this.getAttribute('placeholder');
	}

	set placeholder(val) {
		if (val) {
			this.setAttribute('placeholder', '');
			if(!this.content.length) {
				this.innerHTML = this.compileSource(val);
			}
		} else {
			this.removeAttribute('placeholder');
		}
	}

	compileSource(val) {
		const md = window.markdownit();
		return md.render(val);
	}

	getRawContent() {
		return this.innerText;
	}

	focusCallback(event) {
		this.setAttribute('contenteditable', true);
		this.innerHTML = this.content.length ? this.content : '';
	}

	blurCallback(event) {
		this.content = this.getRawContent();
		this.innerHTML = this.content.length ? this.compileSource(this.content.trim()) : this.placeholder;
		this.setAttribute('contenteditable', false);
	}

	attributeChangedCallback(attrName, oldVal, newVal) {

	}
}

export default MarkDownContent;